package macros

import cats.Functor
import lens.{Lens, VanLaarhovenLens}

import scala.quoted.{Expr, Quotes, Type}


object Macros {

    inline def genLens[S, T](inline lambda: S => T): Lens[S, T] = ${ genLensImpl('lambda) }

    private def genLensImpl[S: Type, T: Type](lambda: Expr[S => T])(using qctx: Quotes): Expr[Lens[S, T]] = {
        import qctx.reflect._
        val sourceSymbol = TypeRepr.of[S].typeSymbol
        if (!sourceSymbol.isClassDef || !sourceSymbol.flags.is(Flags.Case)) {
            scala.sys.error("Only case classes supported")
        }
        val lTerm = lambda.asTerm.underlyingArgument
        val fieldName: String = lTerm match {
            case Lambda(_, Select(_, fieldName)) => fieldName
            case _ => scala.sys.error("Wrong lambda structure")
        }

        val result = '{
            new VanLaarhovenLens[S, T] {
                override def modifyF[F[_] : Functor](f: T => F[T])(s: S): F[S] = {
                    val newVal = f(${Select.unique(('s).asTerm, fieldName).asExprOf[T]});
                    val result = summon[Functor[F]].map(newVal)( (v: T) => ${Select.overloaded(('s).asTerm, "copy", List.empty, List(NamedArg(fieldName, ('v).asTerm))).asExprOf[S]});
                    result
                }
            }
        }
        result
    }
}
