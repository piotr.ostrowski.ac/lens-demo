package app

import scala.util.Random

trait Util {

    def generateIron(n: Int): List[Atom] = {
        List.fill(n) {
            Atom(
                element = Element.Iron,
                position = Point(
                    x = Random.between(-0.1, 0.1),
                    y = Random.between(-0.1, 0.1),
                    z = Random.between(-0.1, 0.1)
                )
            )
        }
    }

}
