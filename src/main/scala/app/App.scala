package app

import cats.Functor
import cats.syntax.functor.*
import lens.{Lens, Traversal, VanLaarhovenLens}
import macros.Macros.genLens

case class Point(x: Double, y: Double, z: Double)

enum Element {
    case Hydrogen, Oxygen, Helium, Iron, Gold
}

case class Atom(element: Element, position: Point)

object App extends Util {

    private def simpleLensDemo: Unit = {
        val p = Point(1.0, 2.0 , 3.0)
        val a = Atom(Element.Hydrogen, p)
        pprint.pprintln(p)

        // easy way to get lens
        // writing 'genLens' is hard
        val pToZ: Lens[Point, Double] = genLens[Point, Double](_.z)

        // Hard way, direct van Laarhoven representation
        val aToP = new VanLaarhovenLens[Atom, Point] {
            override def modifyF[F[_] : Functor](f: Point => F[Point])(s: Atom): F[Atom] = {
                f(s.position).map(newPosition => s.copy(position = newPosition))
            }
        }
        val aToZ = aToP.composeLens(pToZ)

        println(aToZ.get(a))
        val p2 = aToZ.set(6.0)(a)
        pprint.pprintln(p2)

    }

    private def traversalDemo: Unit = {
        val iron: List[Atom] = generateIron(3)
        val allIron: Traversal[List[Atom], Atom] = Traversal.listTraversal[Atom]
        pprint.pprintln(allIron.getAll(iron))
        val mover = allIron
          .composeLens(genLens[Atom, Point](_.position))
          .composeLens(genLens[Point, Double](_.x))
          .modify(x => x + 10)

        val movedIron = mover(iron)
        pprint.pprintln(movedIron)

        val midas = allIron
          .composeLens(genLens[Atom, Element](_.element))
          .replace(Element.Gold)

        val gold = midas(iron)
        pprint.pprintln(gold)
    }

    def main(args: Array[String]): Unit = {
        println("Hello World")
        simpleLensDemo
        traversalDemo
    }
}
