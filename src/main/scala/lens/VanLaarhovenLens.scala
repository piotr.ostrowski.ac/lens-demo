package lens

import cats.Functor
import cats.Id
import cats.data.Const

trait VanLaarhovenLens[S, T] extends Lens[S, T] {

    self =>

    override def modifyF[F[_]: Functor](f: T => F[T])(s: S): F[S]

    override def get(s: S): T = modifyF(t => Const(t))(s).getConst

    override def set(newVal: T)(s: S): S = modifyF[Id](_ => newVal)(s)

    override def modify(f: T => T)(s: S): S = modifyF[Id](f)(s)

    override def composeLens[U](other: Lens[T, U]): Lens[S, U] =
        VanLaarhovenLens( [F[_]] => (f: Functor[F]) ?=> self.modifyF.compose(other.modifyF))

}

object VanLaarhovenLens {
    def apply[S, T](modifier: [F[_]] => Functor[F] ?=> (T => F[T]) => S => F[S]): VanLaarhovenLens[S, T] =
        new VanLaarhovenLens[S, T] {
            override def modifyF[F[_] : Functor](f: T => F[T])(s: S): F[S] = modifier(f)(s)
        }
}