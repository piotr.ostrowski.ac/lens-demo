package lens

import cats.Functor

trait Lens[S, T] {

    def get(s: S): T

    def set(newVal: T)(s: S): S

    def modify(f: T => T)(s: S): S

    def modifyF[F[_]: Functor](f: T => F[T])(s: S): F[S]
    
    def composeLens[U](other: Lens[T, U]): Lens[S, U]

}
