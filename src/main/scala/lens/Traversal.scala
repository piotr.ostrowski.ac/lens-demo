package lens

import cats.{Applicative, Id, Monoid}
import cats.data.Const
import cats.data.Const.catsDataApplicativeForConst

trait Traversal[S, T] {

    self =>

    def modifyA[A[_]: Applicative](f: T => A[T])(s: S): A[S]

    def foldMap[M: Monoid](f: T => M)(s: S): M = modifyA(t => Const[M, T](f(t)))(s).getConst

    def getAll(s: S): List[T] = foldMap(List(_))(s)

    def modify(f: T => T)(s: S): S = modifyA[Id](f)(s)

    def replace(t: T)(s: S) = modifyA[Id](_ => t)(s)

    def composeTraversal[U](other: Traversal[T, U]): Traversal[S, U] = new Traversal[S, U] {
        override def modifyA[A[_]: Applicative](f: U => A[U])(s: S): A[S] = self.modifyA(other.modifyA(f))(s)
    }

    def composeLens[U](other: Lens[T, U]): Traversal[S, U] = new Traversal[S, U] {
        override def modifyA[A[_]: Applicative](f: U => A[U])(s: S): A[S] = self.modifyA(other.modifyF(f))(s)
    }
}

object Traversal {

    def listTraversal[T] = new Traversal[List[T], T] {
        override def modifyA[A[_]: Applicative](f: T => A[T])(s: List[T]): A[List[T]] = {
            s.foldRight(summon[Applicative[A]].pure(List.empty[T]))( (elem, acc) => summon[Applicative[A]].map2(f(elem), acc)(_ :: _) )
        }
    }
}